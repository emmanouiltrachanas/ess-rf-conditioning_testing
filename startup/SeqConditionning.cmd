epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES" "500000") 
require SeqConditionning, agaget
require SimuPulse, 0.2.0
seq ConditionningWF "power=Labo-Simu:Height,countPulse=Labo-Simu:CountPulse,width=Labo-Simu:Width,defaultMajor=Labo-Simu:DefaultMajor,defaultMinor=Labo-Simu:DefaultMinor,defaultCritic=Labo-Simu:DefaultCritic,Experiment=Labo-Simu"

dbLoadRecords("SimuPulseDemo.template","PREFIX=Labo")
dbLoadTemplate("SimuPulse.substitutions")
dbLoadRecords("SeqConditionningDemo.template","EXPERIMENT=Labo-Simu")
dbLoadRecords("SeqConditionning.template","EXPERIMENT=Labo-Simu")

