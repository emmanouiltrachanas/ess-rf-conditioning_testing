epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES" "500000") 
epicsEnvSet("EPICS_CA_ADDR_LIST" "132.166.34.82") 
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST" "NO") 
require SeqConditionning, agaget
require SimuPulse, agaget
require SaveRestore, agaget
require DataAcquisition, agaget
seq SequenceLoad "defect=Labo-Simu:DefaultCritic,loadFile=Labo-Simu:PathSav,load=Labo-Simu:Restore.PROC,isSeqStarted=Labo-Simu:Rdy-Seq,Experiment=Labo-Simu"
seq ConditionningWF "power=Labo-Simu:Height,countPulse=Labo-Simu:CountPulse,width=Labo-Simu:Width,defaultMajor=Labo-Simu:DefaultMajor,defaultMinor=Labo-Simu:DefaultMinor,defaultCritic=Labo-Simu:DefaultCritic,Experiment=Labo-Simu"


SaveRestoreConfigure("$(REQUIRE_SaveRestore_PATH)" )
SaveRestoreDebug(1)
dbLoadRecords("SaveRestoreC.template","PREFIX=Labo-Simu")
dbLoadRecords("SimuPulseDemo.template","PREFIX=Labo")
dbLoadRecords("SeqLoad.template","EXPERIMENT=Labo-Simu")
dbLoadTemplate("SimuPulse.substitutions")
dbLoadRecords("SeqConditionningDemo.template","EXPERIMENT=Labo-Simu")
dbLoadRecords("SeqConditionning.template","EXPERIMENT=Labo-Simu")
dbLoadRecords("DataAcquisitionChannel.template","SECTION=Labo,SUBSECTION=Simu,DISC=disc,DEVICE=device,SIGNAL=signal,SAMPLING_RATE_PV=Labo-Simu:disc-device:signal:SRATE,SIZE=16384,DESCRIPTION=,NELM=16384")

iocInit

dbpf Labo-Simu:PathReq "/opt/epics/modules/SimuPulse/agaget/misc/test.req"
dbpf Labo-Simu:PathReq "/opt/epics/modules/SimuPulse/agaget/misc/test.req"
dbpf Labo-Simu:Pretrig -100
dbpf Labo-Simu:nsamples 1000

dbpf Labo-Simu:Seq-Dir "/home/agaget/test/Sequences"

dbpf Labo-Simu:disc-device:signal:HTRESHOLD 100
dbpf Labo-Simu:disc-device:signal:LTRESHOLD 1

dbpf Labo-Simu:PathSav "/home/agaget/test/Sequences/Seq1.sav"
dbpf Labo-Simu:BW-Hg 80
dbpf Labo-Simu:BW-Low 50
dbpf Labo-Simu:BW-OK 1
